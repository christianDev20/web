
<!doctype html>
<html lang="es">
<head>

    <meta charset="UTF-8">
    <meta class="description" content="Carro de Compra PHP">
    <meta name="viewport" content="width:device-width, initial-scale=1.0">

    <title>CarroCompraPHP</title>

    <link rel="stylesheet" href="css/CarroEstilo.css">
</head>
<body>


    <div id="Contenedor">

        
        <header>

           <div id="primero">
            
                <div id="uno">
                    <img class="logo" src="imagenes/LogoLibro.jpg">
                </div>

                <div id="dos">
                    <input class="texto" type="text" placeholder="Búsque aquí por toda la tienda"/>
                </div>  
                <div style="clear: both;"></div>
           </div>



           <div id="segundo">
                
                <div id="tres">
                    <a href="login.php"><img class="imgLogin" src="imagenes/Login.png" alt=""></a>
                </div>

                <div id="cuatro">
                    <a href="carritodecompras.php"><img class="imgCarroCompra" src="imagenes/CarroCompra.png" alt=""></a>
                </div>
                <div style="clear: both;"></div>
           </div>
           <div style="clear: both;"></div>
        </header>
        <!-- ---------------------------------------------------------------------------------------- -->


        <nav>

            <div id="cinco">
                
                <nav id="navUno">

                    <a class="primero" href="#">Home</a>
                    <a class="segundo" href="#">Nosotros</a>
                    <a class="tercero" href="eventos.php">Eventos</a>
                    <a class="Cuarto" href="ListadoLibros.php">Tiendas FoxBooks</a>

                    <a class="Quinto" href="#"><button id="boton1">Subscríbete</button></a>
                </nav>
            </div>
        </nav>
        <!-- ---------------------------------------------------------------------------------------- -->


        <main>

            <section class="banner">
                <div class="slideshow-container">
                  <div class="mySlides fade">
                    <img src="imagenes/banner1.jpg" style="width:100%; height:300px;">     
                  </div>
                  <div class="mySlides fade">
                    <img src="imagenes/banner2.jpg" style="width:100%; height:300px; ">
                  </div>
                  <div class="mySlides fade">
                    <img src="imagenes/banner3.jpg" style="width:100%; height:300px;">
                  </div>
                  
                  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                  <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
            </section>


            <section id="Secdos">
                
                    <div>
                        <p>LIBROS MÁS VENDIDOS</p>
                    </div>
                


                    <article>

                        <div class="categ"><a class="a1" href="#">General</a></div>
                        <div class="categ"><a class="a1" href="#">Ficción</a></div>
                        <div class="categ"><a class="a1" href="#">Novelas</a></div>
                        <div class="categ"><a class="a1" href="#">Infantil Juvenil</a></div>
                        <div style="clear: both;"></div>
                    </article>  
            </section> 
        </main>
        
    </div>
    


    <footer>

                <div id="divIzquierda">
                    <p id="fotIzquierda">FoxBooks</p>
                </div>

                <div id="divDerecha">
                    <p id="fotDerechaArriba">CONTACTO:<br>
                     399-1860(Anexo 10)(Lun-Vie 9 a.m. a 5 p.m.) Oficina administrativa
                     </p>

                    <p id="fotDerechaAbajo">SÍGUENOS:<br>
                    atencion@crisol.com.pe
                    </p>
                </div>
                <div style="clear:both"></div>  
    </footer>


    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/slider.js"></script>



</body>
</html>